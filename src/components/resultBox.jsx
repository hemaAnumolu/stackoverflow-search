import React, { Component } from 'react';
import TagHtml from './tags.jsx';
import AcceptedAnswer from './acceptedAnswer.jsx';
import { Container, Row, Col } from 'reactstrap';


export default class ResultBox extends Component {
   	constructor( props ) {
	    super(props);	
	}
	render() {
		// converting the EPOC time 
		let askedTime= this.props.time;
		if(askedTime<10000000000) askedTime *= 1000; 
		let d = new Date(askedTime);
		return (
				<Col sm="12" xs="12">
					<h3><a href={"/answer/"+this.props.link}><span dangerouslySetInnerHTML={{__html: this.props.title}} /></a></h3>
					<p>No of Answers: {this.props.answer_count} <br/> Score:{this.props.score} <br/> Views: {this.props.views} </p>
					Tags: <TagHtml tags={this.props.tags}/>
					<AcceptedAnswer answers={this.props.answers} />
					<br/>
					<span>Question Created Date : {d.toLocaleString("en-US")}</span>
				</Col>
			)
	}

}