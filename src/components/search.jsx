import React, { Component } from 'react';
import ResultBox from './resultBox.jsx';
import {Container, Row, Col, FormGroup, Input, Button } from 'reactstrap';


export default class Search extends Component {
   	constructor( props ) {
	    super(props);	
	    //console.log(props);
	    this.state={results : [], keyword:''};
	    //binding the event handlers to this
	    this.handleEnter = this.handleEnter.bind(this); // handles EnterKey on input field
	    this.handleClick = this.handleClick.bind(this); // Handles Search Button Click
	}
	componentDidMount() {
		// handling search URL routing  /search/keyword
	  const { match: { params } } = this.props;
	  //console.log(params.search);
	  if(typeof(params.search) != "undefined")	{
	  		this.state.keyword = params.search;
	  		document.getElementById('search').value = this.state.keyword;
	  		this.getSearchData(this.state.keyword);
		}
	  
	}
	handleEnter(e) {
		if (e.key === 'Enter') {
			this.state.keyword = e.target.value;
			this.props.history.push('/search/'+this.state.keyword);
			this.getSearchData(this.state.keyword);	
		}
	}
	handleClick() {
		this.state.keyword = document.getElementById('search').value;
		this.props.history.push('/search/'+this.state.keyword);
		this.getSearchData(this.state.keyword);	
	}
	getSearchData(){
		// API call to get the results
		fetch('https://api.stackexchange.com/2.2/search?order=desc&sort=activity&intitle='+this.state.keyword+'&site=stackoverflow&filter=!4*mUMjWu4k_.Tn5VX&key=flOnq6oO5P5CSsnX*17kug((')
		    .then(results=> {
		      return results.json();
		    }).then(data=> {
		    	//console.log(data);
		    	let res = data.items.map((result) => {
			       		return <ResultBox 
			       			key={result.question_id} 
			       			link={result.question_id} 
			       			title= {result.title} 
			       			answer_count = {result.answer_count}
			       			score={result.score} 
			       			views={result.view_count} 
			       			tags={result.tags} 
			       			isAnswered={result.is_answered} 
			       			answers={result.answers}
			       			time={result.creation_date}
			       	/>
			      });
			      this.setState({results : res});
		    });	
	}
 

	render() {
	    return (<div>
	    			<Container>
	    				<Row>
			    			<div className="headSection">
			    				<h1>Search Stackoverflow</h1>
			    				<FormGroup>
		          					<input type="text" name="search" id="search" placeholder="Search Stackoverflow" onKeyPress={this.handleEnter}/>
		          					<Button onClick={this.handleClick}>Search</Button>
		          				</FormGroup>
		          			</div>
		    			</Row>
		    			<Row>
		    				{this.state.results}
		    			</Row>
		    		</Container>
	    		</div>
	    	);
	}
}






