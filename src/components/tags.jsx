import React, { Component } from 'react';

export default class TagHtml extends Component {
	constructor(props) {
		super(props)
	}
	render() {
		 return (
		 		
		 		<ul className="tags">
                {this.props.tags.map(function(name, index){
                    return <li key={ index } className="tag">{name}</li>;
                  })}
            	</ul>
		 	)
	}

}