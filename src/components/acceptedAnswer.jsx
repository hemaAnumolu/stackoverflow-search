import React, { Component } from 'react';
import AnswerElement from './answerElement.jsx';
import { Collapse, Button} from 'reactstrap';

export default class AcceptedAnswer extends Component {
	constructor(props) {
		super(props);
		this.toggle = this.toggle.bind(this);
		this.state = { collapse: false };
	}

	toggle() {
	    this.setState({ collapse: !this.state.collapse });
	}

	render() {
		//Dispplaying accepted answer if exists
		if(typeof(this.props.answers) != 'undefined'){
			let acceptedAns	= this.props.answers.find(function(a){
				return a.is_accepted == true;
			})
			if(acceptedAns != null){
				return (
					<div>
						<Button onClick={this.toggle} style={{ marginBottom: '1rem' }}> View Accepted Answer </Button>
						<Collapse isOpen={this.state.collapse}>
							<AnswerElement body={acceptedAns.body} />
						</Collapse>
					</div>
					)
			} 
			return '';
		}
		return '';

		
	}
}