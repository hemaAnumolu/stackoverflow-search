import React, { Component } from 'react';
import AnswerElement from './answerElement.jsx';
import { Container, Row, Col } from 'reactstrap';

export default class AnswerList extends Component {
	constructor(props) {
		super(props);
		this.state={results:[],title:'',numOfAnswers:0};
	}
	componentDidMount() {
	  const { match: { params } } = this.props;
	  this.getAnswers(params.qid);
	  
	}
	getAnswers(qid) {
		//APi to get all Ansers for the gived QuestionID
		fetch('https://api.stackexchange.com/2.2/questions/'+qid+'/answers?order=desc&sort=activity&site=stackoverflow&filter=!-*jbN.OXKfDP&key=flOnq6oO5P5CSsnX*17kug((')
		    .then(results=> {
		      return results.json();
		    }).then(data=> {
		    	 this.setState({numOfAnswers: data.items.length})
		    	if(this.state.numOfAnswers>0){
			    	let res = data.items.map((result,i) => {
				       		return (<div className="answerBox" key={result.answer_id}><h4>Answer {i+1}:</h4><AnswerElement body={result.body}  /></div>)
				      });
				      this.setState({results : res});
				}

		    });
		   //API to get Question Title using QuestionID
		fetch('https://api.stackexchange.com/2.2/questions/'+qid+'?order=desc&sort=activity&site=stackoverflow&key=flOnq6oO5P5CSsnX*17kug((')
			.then(results=> {
		      return results.json();
		    }).then(data=> {
		    	if(data.items.length > 0)
		    	this.setState({title: data.items[0].title})
		    	else this.setState({title: "wrong QuestionID"})
		    });
	}
	render() {
		return (
				<Container>
					<h2> {this.state.title}</h2>
					<h4>Number of Answers Available: {this.state.numOfAnswers}</h4>
					<div id="answerList">{this.state.results}</div>
				</Container>
			)
	}
}