import React from 'react';
import { render } from 'react-dom';
import Search from './components/search.jsx';
import AnswerList from './components/answerList.jsx';
import { BrowserRouter as Router , Switch, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css'; // using boostrap for some fancy styles
import styles from './scss/app.scss'; 
//React routing	
render((
	<Router>
		<div>
	        <Route exact = {true} path="/" component={Search}/>
	        <Route path="/search/:search" component={Search}/>
	        <Route path="/answer/:qid" component={AnswerList}/>
	    </div>
	</Router>
	),
  document.getElementById('root')
);