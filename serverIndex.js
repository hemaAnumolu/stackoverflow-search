var express = require('express')
var app = express()
var path = require('path');
app.set('port', 3000)
app.use(express.static('public'))


app.get('*', function(request, response) {
  response.sendFile(path.join(__dirname + '/src/index.html'));
})

app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port'))
})
